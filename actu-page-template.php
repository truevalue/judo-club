<?php
/**
 * Template Name: Page actus
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
 $query = new WP_Query( [
	 "post_type"      => "post",
	 'posts_per_page' => 10

 ]);
get_header(); ?>

<div class="container margin">
    <h1><?php the_title() ?></h1>
    <div class="col_12 wysiyg">
		<?php
		if ( $query->have_posts() ):
			while ( $query->have_posts() ):
				$query->the_post();

				$categories = wp_get_post_categories( get_the_ID() );
				if ( sizeof( $categories ) > 0 ) {
					$catIcon = get_field( 'icone', get_term( $categories[0] ) );
				} else {
					$catIcon = null;
				}
				?>
                <a class="actu-card" href="<?php the_permalink(); ?>">
					<?php if ( $catIcon ): ?>
                        <img src="<?= $catIcon ?>" alt="" class="actu-card__icon">
					<?php endif; ?>
                    <div class="actu-card__content">
                        <h3><?php the_title() ?></h3>
                        <p><?php the_excerpt(); ?></p>
                    </div>

                </a>
				<?php
			endwhile;
			?>
            <div class="pagination">
				<?php pagination($query->max_num_pages, 2); ?>
            </div>
			<?php
		endif;
		?>
    </div>
</div>
<?php

get_footer();
?>
