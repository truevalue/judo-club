<?php
add_action( 'after_setup_theme', 'register_judo_menu');
add_action("init", "judo_theme_options");
add_action( 'admin_menu', 'custom_menu_page_removing' );
add_image_size("header_cover", 1920);

function register_judo_menu() {

	$locations = array(
		'primary'  =>  __( 'Menu principal', 'judoclub' ),
	);

	register_nav_menus( $locations );
}

function judo_theme_options() {
	acf_add_options_page( [
		'page_title' => __('Options du site'),
		'menu_slug' => 'options-site',
		'post_id' => 'home-options',
		'capability' => 'edit_posts',
		'position' => 30,
		'icon_url' => "dashicons-admin-generic",
	] );
}

function custom_menu_page_removing() {
	remove_menu_page( "edit-comments.php" );
}

add_theme_support( 'post-thumbnails' );
