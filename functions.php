<?php

require_once "inc/admin.php";
require_once "inc/front.php";

add_action("wp_enqueue_scripts", "add_styles");

function add_styles() {
    require_once "dist/imports.php";
}
