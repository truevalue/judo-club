<?php
get_header();
if ( have_posts() ):
	while ( have_posts() ):
		the_post();
		?>
		<?php if ( ! empty( get_the_post_thumbnail_url() ) ): ?>
        <div class="home-banner" style="background-image: url(<?= get_the_post_thumbnail_url(null, "header_cover") ?>)">
        </div>
	<?php endif; ?>

        <div class="container  <?= empty( get_the_post_thumbnail_url() ) ? 'margin' : 'float' ?>">
            <h1><?php the_title() ?></h1>
            <p class="col_12"><?php the_field( "description" ); ?></p>
            <div class="gallery">
				<?php foreach ( get_field( "images" ) as $image ): ?>
                    <div class="gallery__item col_6">
                        <img src="<?= $image['sizes']['medium_large'] ?>" alt="<?= $image['alt'] ?>"
                             class="gallery__item__img" data-lightbox="gallery"
                             data-lightbox-src="<?= $image['url'] ?>"
                             data-lightbox-title="<?= $image['caption'] ?>"
                        >
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
	<?php
	endwhile;
endif;
get_footer();
?>
