import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import markerIconUrl from 'leaflet/dist/images/marker-icon.png';
import markerShadowUrl from 'leaflet/dist/images/marker-shadow.png';

const tileUrl = 'https://tile.thunderforest.com/neighbourhood/{z}/{x}/{y}.png?apikey=678a1cf51be844e9bb9da7dbb6838bb6';
const coords = { long: 48.0332619, lat: 0.2398936 };

const markerIcon = L.icon({
  iconSize: [25, 41],
  iconAnchor: [10, 41],
  popupAnchor: [2, -40],
  // specify the path here
  iconUrl: markerIconUrl,
  shadowUrl: markerShadowUrl,
});

function loadMap() {

  const mapContainer = document.getElementById("contact-map")
  if(mapContainer) {
  const map = new L.Map('contact-map', {
    center: new L.LatLng(coords.long, coords.lat),
    zoom: 18,
  });

  L.marker(
    [coords.long, coords.lat],
    {
      icon: markerIcon,
    },
  ).addTo(map);
  // create a new tile layer
  const layer = new L.TileLayer(tileUrl, { maxZoom: 18 });
  // add the layer to the map
  map.addLayer(layer);
  }

}
window.addEventListener('DOMContentLoaded', loadMap);
