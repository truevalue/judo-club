export default class Lightbox {
    private static Instances: {[x: string]: Lightbox} = {};
    private _imgs: LightboxImage[] = [];
    private _el: LightboxElement;

    constructor(imgs: HTMLElement[]) {
        imgs.forEach(img => {
            const imgData = {
                src: img.getAttribute("data-lightbox-src"),
                title: img.getAttribute("data-lightbox-title")
            };
            const index = this._imgs.push(imgData) - 1;
            img.addEventListener("click", () => {
                this.index = index
            })
        });
        this._el = new LightboxElement(this)

    }

    private _index: number = 0;

    get index(): number {
        return this._index;
    }

    set index(value: number) {
       const max = this._imgs.length;
         value = ((value % max) + max)%max;
        this._index = value;
        this.open = true;
        this._el.image = this._imgs[this.index].src;
        this._el.title = this._imgs[this.index].title
    }

    get _img(): LightboxImage {
        return this._imgs[this._index]
    }

    set open(value: boolean) {
        this._el.open = value
    }

    public static Init() {
        const els = document.querySelectorAll("[data-lightbox]");
        const elsList = new Hash<HTMLElement>();
        for (let i = 0; i < els.length; i++) {
            elsList.push(<HTMLElement>els[i], els[i].getAttribute('data-lightbox'))
        }
        elsList.forEach((el, index) => {
            Lightbox.Instances[index] = new Lightbox(el)
        })
    }
}

class LightboxElement {
    private _background: HTMLElement;
    private _closeCross: HTMLElement;
    private _inner: HTMLElement;
    private _lightbox: Lightbox;
   private readonly _arrowNext: HTMLElement;
   private readonly _arrowPrev: HTMLElement;

    constructor(lightbox: Lightbox) {
       this._lightbox = lightbox;

        this._background = document.createElement('div');
        this._inner = document.createElement('div');
        this._background.classList.add("lightbox__background");
        this._inner.classList.add("lightbox__inner");
        this._closeCross = document.createElement('div')
        this._closeCross.classList.add("lightbox__close")
        this._closeCross.addEventListener("click", () => {
                this.open = false
        });
        this._inner.appendChild(this._closeCross)
        this._background.addEventListener("click", event => {
           if(event.target === this._background){
              this.open = false
           }
        });
        this._background.appendChild(this._inner);
        this._image = new Image;
        this._image.onload = this.load.bind(this);
        this._inner.appendChild(this._image);
        this._title = document.createElement("div");
        this._title.classList.add('lightbox__label')
        this._inner.appendChild(this._title);

       this._arrowPrev = document.createElement("div");
       this._arrowPrev.classList.add("lightbox__arrow");
       this._arrowPrev.classList.add("lightbox__arrow--prev");
       this._arrowPrev.addEventListener("click", this.prev.bind(this));
       this._background.appendChild(this._arrowPrev);

       this._arrowNext = document.createElement("div");
       this._arrowNext.classList.add("lightbox__arrow");
       this._arrowNext.classList.add("lightbox__arrow--next");
       this._arrowNext.addEventListener("click", this.next.bind(this));
       this._background.appendChild(this._arrowNext);


       document.querySelector("body").appendChild(this._background)
    }

    public prev() {
       this._lightbox.index--
    }
    public next() {
       this._lightbox.index++
    }

    private _title: HTMLElement;

    set title(value: string) {
        if(value) {
            this._title.classList.remove("hidden")
        } else {
            this._title.classList.add("hidden")
        }
        this._title.innerText = value
    }

    private _image: HTMLImageElement;

    set image(path: string) {
        this.loading = true;
        setTimeout(()=>{
            this._image.src = path
        }, 300)
    }

    set open(value: boolean) {
        if (value) {
            this._background.classList.add("opened");
        } else {
            this._background.classList.remove("opened");
        }
    }

    set loading(value: boolean) {
        if (value) {
            this._inner.classList.add("loading")
        } else {
            this._inner.classList.remove("loading")
        }
    }

    load() {
        this._inner.style.width = this._image.width + 40 + "px";
        this._inner.style.height = this._image.height + 40 + "px";
        this.loading = false;
    }

}

interface LightboxImage {
    src: string,
    title?: string,
}

class Hash<Type> {
    private _els: {[kex: string]: Type[]} = {};

    get keys() {
        return Object.keys(this._els)
    }

    public push(el: Type, key: string) {
        if (!this._els[key]) {
            this._els[key] = []
        }
        this._els[key].push(el)
    }

    public forEach(callback: (el: Type[], index: string) => void) {
        this.keys.forEach(key => {
            callback(this._els[key], key)
        })
    }
}
